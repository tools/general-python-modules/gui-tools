def elementValue(getter, setter, val):
    if val is None:
        return getter()
    else:
        setter()


def sliderValue(slider, val):
    return elementValue(lambda: slider.value(),
                        lambda: slider.setValue(val),
                        val)


def lineEditValue(lineEdit, val, datatype=str):
    return elementValue(lambda: datatype(lineEdit.text()),
                        lambda: lineEdit.setText(str(val)),
                        val)


def radioButtonState(radioButton, val):
    return elementValue(lambda: radioButton.isChecked(),
                        lambda: radioButton.setChecked(val),
                        val)


def comboBoxSelection(comboBox, val):
    return elementValue(lambda: comboBox.currentText(),
                        lambda: comboBox.setCurrentIndex(
                            comboBox.findText(val)),
                        val)


def checkBoxState(checkBox, val):
    return elementValue(lambda: checkBox.isChecked(),
                        lambda: checkBox.setChecked(val),
                        val)
